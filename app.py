from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from config import Config
from werkzeug.utils import secure_filename

app = Flask(__name__)

app.config.from_object(Config)
db = SQLAlchemy(app)

from route import app_route
app.register_blueprint(app_route)

if __name__ == '__main__':
    app.run(debug=True)

