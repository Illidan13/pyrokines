from app import db

class Albums(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    alb_name = db.Column(db.String(100), nullable=False)
    alb_img = db.Column(db.String(100), nullable=False)

    def __repr__(self):
        return '<Albums %r>' % self.id