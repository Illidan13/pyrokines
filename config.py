import os

class Config:
    DEBAG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///pyrokinesis.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False