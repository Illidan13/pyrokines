new Swiper('.main-albums-swiper', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    scrollbar: {
        el: '.main-albums-swiper-scrollbar',
        draggable: true
    },
    slidesPerView: 3,
    loop:true
});