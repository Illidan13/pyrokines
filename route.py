from flask import render_template, url_for, request, redirect, flash, Blueprint
from models.Albums import Albums
from werkzeug.utils import secure_filename
from datetime import datetime

app_route = Blueprint('route', __name__)

@app_route.route('/')
@app_route.route('/home')
def index():
    albums = Albums.query.order_by(Albums.alb_name).all()
    return render_template("index.html", albums=albums)

@app_route.route('/about')
def about():
    return render_template("about.html")


@app_route.route('/create-album', methods=['POST', 'GET'])
def create_album():
    if request.method == "POST":
        name = request.form['name']
        photo = "/static/Images/albums/" + request.form['file']
        album = Albums(alb_name=name, alb_img=photo)

        try:
            db.session.add(album)
            db.session.commit()
            return redirect('/')
        except:
            return "WATAFAK IS A KILOMETER!!!!!"
    else:
        return render_template("create-album.html")
